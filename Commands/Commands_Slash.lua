
chanvar = "GUILD";

function MangAdmin:outSAY(text)
 SendChatMessage("."..text, "GUILD", nil,nil);--functional type2
 --SendChatMessage(""..text, "GUILD", nil,nil);--test type2
 --SendChatMessage(""..text, "SAY", nil,nil);--test type2noguild
 --SendChatMessage(text, "GUILD", nil,nil);--old]]
 --SendChatMessage(""..text, chanvar, nil, nil);
end

function GMH_Main()
DEFAULT_CHAT_FRAME:AddMessage("--TrueWoW GM Helper --",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/gm            - Displays Current Menu",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/reload        - Reloads User Interface",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/revive        - Revive Yourself When Dead",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/recallport    - Teleports To Specified Location",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/npcspawn      - Spawns Specified Creature",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/npcdelete     - Deletes Targeted Creature",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/additem       - Adds Specified Item To Yourself/Targeted Player",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/announce      - Broadcasts Server-Wide Message",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/nameann       - Broadcasts Server-Wide Message w/ Your Name",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/gmannounce    - Broadcasts Message to GM's only.",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/savedannounce - Broadcasts Saved Announces",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/advanceall    - Advances All Skills By A Specified Amount",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/reviveplr     - Revives Specified Player",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/learn         - Learn Specified Spell",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/unlearn       - Unlearn Specified Spell",0.0, 1.0, 0.0, 53, 0);
DEFAULT_CHAT_FRAME:AddMessage("/table         - Reloads Specified Table In Database",0.0, 1.0, 0.0, 53, 0);
end
SlashCmdList["GMH"] = GMH_Main;
SLASH_GMH1="/gmh";
SLASH_GMH2="/gmhh";

function GMH_Horde()
SendWho('r-orc r-undead r-tauren r-troll r-blood');
end
SlashCmdList["HORDE"] = GMH_Horde;
SLASH_HORDE1="/horde";

function GMH_Alli()
SendWho('r-human r-dwaft r-gnome r-night r-draenei');
end
SlashCmdList["ALLI"] = GMH_Alli;
SLASH_ALLI1="/alli";

function GMH_Dk()
SendWho('c-death 55-60');
end
SlashCmdList["DK"] = GMH_Dk;
SLASH_DK1="/dk";

function GMH_QQ()
QUIT()
end
SlashCmdList["QQ"] = GMH_QQ;
SLASH_QQ1="/q";

function GMH_Who(msg)
local GMWho=msg;
result=".gm i"; 
MangAdmin:ChatMsg(result);
end
SlashCmdList["GMWHO"] = GMH_Who;
SLASH_GMWHO1="/gm";
SLASH_GMWHO2="/gmi";

function GMH_TicketComment(msg)
local GMTicket=msg;
result=".ticket comment "..GMTicket; 
MangAdmin:ChatMsg(result);
end
SlashCmdList["GMHCOMMENT"] = GMH_GMTicketComment;
SLASH_GMHCOMMENT1="/comment";
SLASH_GMHCOMMENT2="/tcomment";

function GMH_Reload()
ReloadUI()
end
SlashCmdList["GMHRELOAD"] = GMH_Reload;
SLASH_GMHRELOAD1="/reload";

function GMH_Revive()
result=".revive"   
MangAdmin:ChatMsg(result);
end
SlashCmdList["REVIVE"] = GMH_Revive;
SLASH_REVIVE1="/revive";

function GMH_NPCSpawn(msg)
local NPCID=msg;
result=".npc add "..NPCID; 
outSAY(result);
end
SlashCmdList["GMHSPAWN"] = GMH_NPCSpawn;
SLASH_GMHSPAWN1="/npcspawn";

function GMH_NPCDelete()
result=".npc delete" 
outSAY(result);
end
SlashCmdList["GMHDELETE"] = GMH_NPCDelete;
SLASH_GMHDELETE1="/npcdelete";

function GMH_AddItem(msg)
local ItemID=msg;
result=".additem "..ItemID; 
outSAY(result);
end
SlashCmdList["GMHADDITEM"] = GMH_AddItem;
SLASH_GMHADDITEM1="/additem";

function GMH_Announce(msg)
local Announce=msg;
result=".announce "..Announce;
outSAY(result);
end
SlashCmdList["GMHANNOUNCE"] = GMH_Announce;
SLASH_GMHANNOUNCE1="/announce";
SLASH_GMHANNOUNCE2="/an";

--function GMH_WAnnounce(msg)
--local WAnnounce=msg;
--result=".mute "..WAnnounce; 
--outSAY(result);
--end
--SlashCmdList["GMHWANNOUNCE"] = GMH_WAnnounce;
--SLASH_GMHWANNOUNCE1="/pmute";
--SLASH_GMHWANNOUNCE2="/testmute";


function GMH_GMAnnounce(msg)
local GMAnnounce=msg;
result=".gmnameannounce "..GMAnnounce; 
outSAY(result);
end
SlashCmdList["GMHGMANNOUNCE"] = GMH_GMAnnounce;
SLASH_GMHGMANNOUNCE1="/gmannounce";
SLASH_GMHGMANNOUNCE2="/gman";

function GMH_RecallPort(msg)
local location=msg;
result=".tele "..location; 
outSAY(result);
end
SlashCmdList["GMHRECALLPORT"] = GMH_RecallPort;
SLASH_GMHRECALLPORT1="/recallport";
SLASH_GMHRECALLPORT2="/recall";
SLASH_GMHRECALLPORT3="/port";

function GMH_SavedAnnounce(msg)
local SavedAnnounce=msg;
if (SavedAnnounce == "1") and (firstannounce ~= nil) then
result=firstannounce;
outSAY(result);
elseif (SavedAnnounce == "2") and (secondannounce ~= nil) then
result=secondannounce;
outSAY(result);
elseif (SavedAnnounce == "3") and (thirdannounce ~= nil) then
result=thirdannounce;
outSAY(result);
elseif (SavedAnnounce == "4") and (fourthannounce ~= nil) then
result=fourthannounce;
outSAY(result);
elseif (SavedAnnounce == "5") and (fifthannounce ~= nil) then
result=fifthannounce;
outSAY(result);
else
UIErrorsFrame:AddMessage("That saved announcement has not been set! Please set it!", 1.0, 0.0, 0.0, 53, 2);
end
end
SlashCmdList["GMHSAVEDANNOUNCE"] = GMH_SavedAnnounce;
SLASH_GMHSAVEDANNOUNCE1="/savedannounce";
SLASH_GMHSAVEDANNOUNCE2="/sa";

function GMH_Learn(msg)
local Learn=msg;
result=".learn "..Learn; 
outSAY(result);
end
SlashCmdList["GMHLEARN"] = GMH_Learn;
SLASH_GMHLEARN1="/learn";

function GMH_UnLearn(msg)
local UnLearn=msg;
result=".unlearn "..UnLearn; 
outSAY(result);
end
SlashCmdList["GMHUNLEARN"] = GMH_UnLearn;
SLASH_GMHUNLEARN1="/unlearn";

function GMH_RevivePlr(msg)
local PlayerName=msg;
result=".reviveplr "..PlayerName; 
outSAY(result);
end
SlashCmdList["GMHREVIVEPLR"] = GMH_RevivePlr;
SLASH_GMHREVIVEPLR1="/reviveplr";

function GMH_AdvanceAllSkills(msg)
local Skillnumber=msg;
result=".maxskill"; 
outSAY(result);
end
SlashCmdList["GMHADVANCEALL"] = GMH_AdvanceAllSkills;
SLASH_GMHADVANCEALL1="/advanceall";
SLASH_GMHADVANCEALL2="/advanceallskills";

function TSR_Sounds(msg)
local Sound=msg
local line='/script PlaySound("'..Sound..'")';
ChatFrameEditBox:SetText("");
ChatFrameEditBox:Insert(line);
ChatEdit_SendText(ChatFrameEditBox);
end
SlashCmdList["TSRSOUND"] = TSR_Sounds;
SLASH_TSRSOUND1="/ps";

function TSR_TableReload(msg)
local Table=msg
result=".reload "..Table;
outSAY(result);
end
SlashCmdList["TSRTR"] = TSR_TableReload;
SLASH_TSRTR1="/tr";
SLASH_TSRTR2="/table";

function GMH_Realm()
result=".unaura 52693"; 
result2=".unaura 52275";
outSAY(result);
outSAY(result2);
end
SlashCmdList["GMHREALM"] = GMH_Realm;
SLASH_GMHREALM1="/realm";
SLASH_GMHREALM2="/realmset";

function GMH_Dkfactionhorde()
result=".quest add 13189 Horde"; 
outSAY(result);
end
SlashCmdList["GMHDKFACTIONHORDE"] = GMH_Dkfactionhorde;
SLASH_GMHDKFACTIONHORDE1="/dkfactionhorde";

function GMH_Dkfactionalli()
result=".quest add 13189 Alliance"; 
outSAY(result);
end
SlashCmdList["GMHDKFACTIONALLI"] = GMH_Dkfactionalli;
SLASH_GMHDKFACTIONALLI1="/dkfactionalli";

function GMH_Evade()
result=".die"; 
result2=".respawn"
outSAY(result);
outSAY(result2);
end
SlashCmdList["GMHEVADE"] = GMH_Evade;
SLASH_GMHEVADE1="/evade";

function GMH_Info(msg)
local RandomDataEntered=msg;
result=".pinfo "..RandomDataEntered;
outSAY(result);
end
SlashCmdList["GMHINFO"] = GMH_Info;
SLASH_GMHINFO1="/info";

function GMH_LookupAccount(msg)
local GMAccount=msg;
result=".lookup player account "..GMAccount; 
outSAY(result);
end
SlashCmdList["GMHLOOKAC"] = GMH_LookupAccount;
SLASH_GMHLOOKAC1="/acclook";

function GMH_Kill()
result=".damage 1000000"
result1=".damage 1000000"
result2=".damage 1000000"
outSAY(result);
outSAY(result1);
outSAY(result2);
end
SlashCmdList["GMHKILL"] = GMH_Kill;
SLASH_GMHKILL1="/kill";

function GMH_Frost()
result=".additem 49426 -2"
outSAY(result);
end
SlashCmdList["GMHFROST"] = GMH_Frost;
SLASH_GMHFROST1="/frost";

function GMH_ClassQuestShamanHorde()
result=".quest add 96"; 
outSAY(result);
end
SlashCmdList["GMHCLASSQUESTSHAMNHORDE"] = GMH_ClassQuestShamanHorde;
SLASH_GMHCLASSQUESTSHAMNHORDE1="/shamanhorde";

function GMH_ClassQuestShamanAlli()
result=".quest add 9509"; 
outSAY(result);
end
SlashCmdList["GMHCLASSQUESTSHAMNHORDE"] = GMH_ClassQuestShamanAlli;
SLASH_GMHCLASSQUESTSHAMNHORDE1="/shamanalli";

function GMH_GMLearnSpells()
result1=".learn 32377"
result2=".learn 30450"
result3=".learn 27371"
result4=".learn 27369"
result5=".learn 27368"
result6=".learn 30360"
result7=".learn 25206"
result8=".learn 30362"
result9=".learn 34923"
result10=".learn 25370"
result11=".learn 25224"
result12=".learn 25220"
result13=".learn 25390"
result14=".learn 25382"
result15=".learn 28277"
result16=".learn 25313"
result17=".learn 27379"
result18=".learn 33942"
result19=".learn 33935"
result20=".learn 32435"
result21=".learn 32443"
result22=".learn 29912"
result23=".learn 27297"
result24=".learn 27304"
result25=".learn 25441"
result26=".learn 785"
result27=".learn 27604"
result28=".learn 27648"
result29=".learn 27646"
result30=".learn 26977"
result31=".learn 26546"
result32=".learn 26540"
result33=".learn 26339"
result34=".learn 26170"
result35=".learn 26139"
result36=".learn 26126"
result37=".learn 25947"
result38=".learn 25840"
result39=".learn 25812"
result40=".learn 25805"
result41=".learn 25772"
result42=".learn 25749"
result43=".learn 25462"
result44=".learn 25425"
result45=".learn 25322"
result46=".learn 25260"
result47=".learn 25050"
result48=".learn 25033"
result49=".learn 24910"
result50=".learn 24843"
result51=".learn 24312"
result52=".learn 22876"
result53=".learn 22865"
result54=".learn 7033"
result55=".learn 17235"
result56=".learn 260"
result57=".learn 18209"
result58=".learn 747"
result59=".learn 1302"
result60=".learn 8985"
result61=".learn 17178"
result62=".learn 40938"
result63=".learn 33329"
result64=".learn 35354"
outSAY(result1);
outSAY(result2);
outSAY(result3);
outSAY(result4);
outSAY(result5);
outSAY(result6);
outSAY(result7);
outSAY(result8);
outSAY(result9);
outSAY(result10);
outSAY(result11);
outSAY(result12);
outSAY(result13);
outSAY(result14);
outSAY(result15);
outSAY(result16);
outSAY(result17);
outSAY(result18);
outSAY(result19);
outSAY(result20);
outSAY(result21);
outSAY(result22);
outSAY(result23);
outSAY(result24);
outSAY(result25);
outSAY(result26);
outSAY(result27);
outSAY(result28);
outSAY(result29);
outSAY(result30);
outSAY(result31);
outSAY(result32);
outSAY(result33);
outSAY(result34);
outSAY(result35);
outSAY(result36);
outSAY(result37);
outSAY(result38);
outSAY(result39);
outSAY(result40);
outSAY(result41);
outSAY(result42);
outSAY(result43);
outSAY(result44);
outSAY(result45);
outSAY(result46);
outSAY(result47);
outSAY(result48);
outSAY(result49);
outSAY(result50);
outSAY(result51);
outSAY(result52);
outSAY(result53);
outSAY(result54);
outSAY(result55);
outSAY(result56);
outSAY(result57);
outSAY(result58);
outSAY(result59);
outSAY(result60);
outSAY(result61);
outSAY(result62);
outSAY(result63);
outSAY(result64);
end
SlashCmdList["GMHGMLEARN"] = GMH_GMLearnSpells;
SLASH_GMHGMLEARN1="/learngmspells";

function GMH_GMUnlearnSpells()
result1=".unlearn 32377"
result2=".unlearn 30450"
result3=".unlearn 27371"
result4=".unlearn 27369"
result5=".unlearn 27368"
result6=".unlearn 30360"
result7=".unlearn 25206"
result8=".unlearn 30362"
result9=".unlearn 34923"
result10=".unlearn 25370"
result11=".unlearn 25224"
result12=".unlearn 25220"
result13=".unlearn 25390"
result14=".unlearn 25382"
result15=".unlearn 28277"
result16=".unlearn 25313"
result17=".unlearn 27379"
result18=".unlearn 33942"
result19=".unlearn 33935"
result20=".unlearn 32435"
result21=".unlearn 32443"
result22=".unlearn 29912"
result23=".unlearn 27297"
result24=".unlearn 27304"
result25=".unlearn 25441"
result26=".unlearn 785"
result27=".unlearn 27604"
result28=".unlearn 27648"
result29=".unlearn 27646"
result30=".unlearn 26977"
result31=".unlearn 26546"
result32=".unlearn 26540"
result33=".unlearn 26339"
result34=".unlearn 26170"
result35=".unlearn 26139"
result36=".unlearn 26126"
result37=".unlearn 25947"
result38=".unlearn 25840"
result39=".unlearn 25812"
result40=".unlearn 25805"
result41=".unlearn 25772"
result42=".unlearn 25749"
result43=".unlearn 25462"
result44=".unlearn 25425"
result45=".unlearn 25322"
result46=".unlearn 25260"
result47=".unlearn 25050"
result48=".unlearn 25033"
result49=".unlearn 24910"
result50=".unlearn 24843"
result51=".unlearn 24312"
result52=".unlearn 22876"
result53=".unlearn 22865"
result54=".unlearn 7033"
result55=".unlearn 17235"
result56=".unlearn 260"
result57=".unlearn 18209"
result58=".unlearn 747"
result59=".unlearn 1302"
result60=".unlearn 8985"
result61=".unlearn 17178"
result62=".unlearn 40938"
result63=".unlearn 33329"
result64=".unlearn 35354"
outSAY(result1);
outSAY(result2);
outSAY(result3);
outSAY(result4);
outSAY(result5);
outSAY(result6);
outSAY(result7);
outSAY(result8);
outSAY(result9);
outSAY(result10);
outSAY(result11);
outSAY(result12);
outSAY(result13);
outSAY(result14);
outSAY(result15);
outSAY(result16);
outSAY(result17);
outSAY(result18);
outSAY(result19);
outSAY(result20);
outSAY(result21);
outSAY(result22);
outSAY(result23);
outSAY(result24);
outSAY(result25);
outSAY(result26);
outSAY(result27);
outSAY(result28);
outSAY(result29);
outSAY(result30);
outSAY(result31);
outSAY(result32);
outSAY(result33);
outSAY(result34);
outSAY(result35);
outSAY(result36);
outSAY(result37);
outSAY(result38);
outSAY(result39);
outSAY(result40);
outSAY(result41);
outSAY(result42);
outSAY(result43);
outSAY(result44);
outSAY(result45);
outSAY(result46);
outSAY(result47);
outSAY(result48);
outSAY(result49);
outSAY(result50);
outSAY(result51);
outSAY(result52);
outSAY(result53);
outSAY(result54);
outSAY(result55);
outSAY(result56);
outSAY(result57);
outSAY(result58);
outSAY(result59);
outSAY(result60);
outSAY(result61);
outSAY(result62);
outSAY(result63);
outSAY(result64);
end